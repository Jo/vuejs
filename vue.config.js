module.exports = {
  "lintOnSave": false,
  "devServer": {      
    "noInfo": true,
    "proxy": {
      "/api": {
        target: "http://pocgenrateimage.jo",
        ws: true,
        changeOrigin: true
      },
      
    }
  },
  "transpileDependencies": [
    "vuetify"
  ]
}