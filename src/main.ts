
import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
//@ts-ignore
import zingchartVue from 'zingchart-vue';
import vuetify from './plugins/vuetify';
import axios from 'axios';

Vue.config.productionTip = false;
Vue.component('zingchart', zingchartVue);


new Vue({
  router,
  store,
  //@ts-ignore
  axios,
  vuetify,
  render: (h: any) => h(App)
}).$mount("#app");

