export const groupBy = <T>(xs: Array<T>, selector: (i: T) => string) => {
  return Object.entries(toStringLookup(xs,selector)).map(it => {return {key:it[0],values:it[1]}})
}

export const groupByNumber = <T>(xs: Array<T>, selector: (i: T) => number) => {
  return Object.entries(toNumberLookup(xs,selector)).map(it => {return {key:it[0],values:it[1]}})
}

export const extractDistinctById = <T,T1>(xs: Array<T>, selector: (i: T) => number, dicItems: {[id: number]:T1} ) => {
  return Object.entries(toNumberLookup(xs,selector)).map(it => dicItems[parseInt(it[0])])
}

export const extractDistinctProp = <T>(xs: Array<T>, selector: (i: T) => string ) => {
  return Object.entries(toStringLookup(xs,selector)).map(it => it[0]).sort()
}

export const toNumberDictionary = <T>(xs: Array<T>, selector: (i: T) => number,buildDefaultItem?:()=>T) => {
  return xs.reduce(function(rv, x) {      
    rv[selector(x)] = buildDefaultItem?{...buildDefaultItem(),...x}:x
    return rv
  }, {} as {[index: number]: T})
}

export const toStringDictionary = <T>(xs: Array<T>, selector: (i: T) => string,buildDefaultItem?:()=>T) => {
  return xs.reduce(function(rv, x) {      
    rv[selector(x)] = buildDefaultItem?{...buildDefaultItem(),...x}:x
    return rv
  }, {} as {[index: string]: T})
}

export const toNumberLookup = <T>(xs: Array<T>, selector: (i: T) => number) => {
  return xs.reduce(function(rv, x) {      
    (rv[selector(x)] = rv[selector(x)] || []).push(x)
    return rv
  }, {} as {[index: number]: Array<T>})
}

export const toStringLookup = <T>(xs: Array<T>, selector: (i: T) => string) => {
  return xs.reduce(function(rv, x) {      
    (rv[selector(x)] = rv[selector(x)] || []).push(x)
    return rv
  }, {} as {[index: string]: Array<T>})
}

export const toGroupDictionnary = <T>(xs: Array<T>, dicSelector: (i: T) => string, grpSelector: (i: T) => string) => {
  return Object.entries(toStringLookup(xs,dicSelector)).reduce((dic,current) => {
      dic[current[0]] = groupBy(current[1],grpSelector)
    return dic
  }, {} as {[index: string]: {key: string;values: T[]}[]})
}

export const toGroupDictionnaryNumber = <T>(xs: Array<T>, dicSelector: (i: T) => string, grpSelector: (i: T) => number) => {
  return Object.entries(toStringLookup(xs,dicSelector)).reduce((dic,current) => {
      dic[current[0]] = groupByNumber(current[1],grpSelector)
    return dic
  }, {} as {[index: string]: {key: string;values: T[]}[]})
}


export const chunk = <T>(xs: Array<T>, chunkSize:number = 2 ) => {  
  if(chunkSize == 0) return []
  return Array.apply(null,Array(Math.ceil(xs.length/chunkSize))).map((x,i) => i).map((x,i) => xs.slice(i*chunkSize,i*chunkSize+chunkSize));
}

export const toSubArray = <T>(xs: Array<T>, nbSubArray:number = 2 ) => {  
  return chunk(xs, Math.ceil(xs.length / nbSubArray))
}

/*
export const chunk = <T>(xs: Array<T>, nbSubArray:number = 2 ) => {  
}
*/