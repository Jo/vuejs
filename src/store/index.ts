import Vue from "vue";
import VueRouter from "vue-router";
import Vuex from "vuex";
import Person from '../models/Person';

Vue.use(Vuex);

interface IAppState {
  people: Person[],
  images: {prediction: string, src: string}[]
}

const state: IAppState = {
  people: [
    new Person("Jean","Dupont"),
    new Person("Charles","Baudelaire"),
    new Person("Brice","DeNice"),
    new Person("Jacques","Brel")
  ],

  images: [
    // {prediction: "", img: "https://www.immigrantspirit.com/wp-content/uploads/2013/11/marienplatz-munich.jpg"},
    {prediction: "", src: "https://images.sudouest.fr/2020/01/21/5e27092366a4bd6733ae5f03/widescreen/1000x500/plus-de-14700-bergers.jpg?v1"},
    {prediction: "", src: "https://upload.wikimedia.org/wikipedia/commons/3/32/Chien_de_Berger_Belge_Malinois.jpg"},
    {prediction: "", src: "https://static.actu.fr/uploads/2019/10/chien.jpg"},
    {prediction: "", src: "https://www.lepointveterinaire.fr/images/57a/c2a2e0cbe5ee7f28ed5e706a08efd.jpg"},
    {prediction: "", src: "https://cdn-s-www.lalsace.fr/images/0CF481AA-CA06-46E5-93B3-990FE3B456BC/NW_raw/l-adolescent-est-mort-apres-avoir-chasse-et-mange-une-marmotte-photo-d-illustration-pixabay-1594757883.jpg"},
    {prediction: "", src: "https://cdn.futura-sciences.com/buildsv6/images/wide1920/2/8/1/281573c9d1_50163673_serpent-min.jpg"},
    {prediction: "", src: "https://upload.wikimedia.org/wikipedia/commons/0/01/2019_Peugeot_508_GT-Line_BlueHDi_1.5_%28130_PS%29_Front.jpg"}
  ]
};


type State = typeof state;

const mutations = {
  ADD_PERSON_SUCCESS(state: State , {name,firstname }:{name:string,firstname:string}) {
    state.people.push(new Person(firstname, name));
  },

  changeData(state: State, {prediction, src} : {prediction:string, src:string} ){
    var i: number = state.images.indexOf(state.images.filter(e => e.src == src)[0]);
    state.images[i].prediction = prediction;
  }
};

const actions = {
  addPerson(context: any,  {name,firstname }:{name:string,firstname:string}) {    
    setTimeout(()=>{
      context.commit("ADD_PERSON_SUCCESS",{name,firstname});
    },700)
  }
};

const getters = {
  getPeople(state: State) {
    return state.people;
  },

  getNoms(state: State) {
    const noms: Array<string> = [];
    state.people.forEach((e: Person) => {
      noms.push(e.nom);
    });
    return noms;
  },

  getPrenoms(state: State) {
    const noms: Array<string> = [];
    state.people.forEach((e: Person) => {
      noms.push(e.prenom);
    });
    return noms;
  },

  getImages(state: State){
    return state.images;
  }
};

  
export default new Vuex.Store({
  state,
  mutations,
  actions,
  getters
});
