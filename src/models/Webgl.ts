import * as mat4 from "gl-matrix/mat4";

export default class Webgl{
    
    public gl: WebGLRenderingContext;
    public vertexSource : string;
    public fragmentSource : string;
    public programInfo;
    public buffers: {vertices: WebGLBuffer, colors: WebGLBuffer, indices: WebGLBuffer};
    public numComponents: {vertex: number, color: number};
    public positions: number[];
    public colors: number[];
    public indices: number[];
    public squareRotation: number;
    public then: number;
    public deltaTime: number;
    public projectionMatrix;
    public modelViewMatrix;
    public Mmatrix;
    
    
    // Props for 3D cube plot
    //https://www.tutorialspoint.com/webgl/webgl_interactive_cube.htm
    public AMORTIZATION = 0.95;
    public drag = false;
    public old_x;
    public old_y;
    public dX = 0
    public dY = 0;
    public THETA = 0;
    public PHI = 0;

    //View part
    public proj_matrix = [];
    public mo_matrix = [ 1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1 ];
    public view_matrix = [ 1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1 ];
    public fieldOfView: number;
    public aspect: number;
    public zNear: number; 
    public zFar: number;
    public translate: number[];
    public rotate: number[];
    public vertexCount;

    constructor(
        gl: WebGLRenderingContext, 
        vertexSource : string, 
        fragmentSource : string, 
        positions: number[],
        numComponents: {vertex: number, color: number},
        vertexCount: number,
        colors?: number[], 
        indices?: number[],
        
    ){
        this.gl = gl;
        this.vertexSource = vertexSource;
        this.fragmentSource = fragmentSource;
        this.positions = positions;
        this.colors = colors;
        this.indices = indices;
        this.numComponents = numComponents;
        this.buffers = {vertices: null, colors: null, indices: null};
        this.squareRotation = 0.0;
        this.then = 0.0;
        this.deltaTime = 0.001;
        this.vertexCount = vertexCount;
        this.proj_matrix = this.get_projection(40, this.gl.canvas.width/this.gl.canvas.height, 1, 100);

        this.mouseEvents();
    }

    /**
     * Initializes the buffers: vertices buffer and colors buffer, which are initially float matrices.
     */
    public initBuffers(){
        
        if (this.positions){
            const positionsBuffer = this.gl.createBuffer();
            this.gl.bindBuffer(this.gl.ARRAY_BUFFER, positionsBuffer);
            this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(this.positions), this.gl.STATIC_DRAW);
            this.buffers.vertices = positionsBuffer;
            if (this.colors){
                const colorBuffer = this.gl.createBuffer();
                this.gl.bindBuffer(this.gl.ARRAY_BUFFER, colorBuffer);
                this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(this.colors), this.gl.STATIC_DRAW);
                this.buffers.colors = colorBuffer;
            }
            if (this.indices){
                const indicesBuffer = this.gl.createBuffer();
                this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, indicesBuffer);
                this.gl.bufferData(this.gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(this.indices), this.gl.STATIC_DRAW);
                this.buffers.indices = indicesBuffer;
            }
            else { 
                console.log(
                `One of color or indices buffer is not defined: \ncolors: ${this.colors} \nindices: ${this.indices}`); 
            }
            // console.log(this.positions, this.colors, this.indices);

        }
        else { throw "Error: positions buffer is not defined";}
    }       

    /**
     * creates a shader of the given type, uploads the source and compiles it.
     * @param type 
     * @param source 
     * @returns 
     */
    public loadShader(type, source): WebGLShader {
        const shader = this.gl.createShader(type);
        // Send the source to the shader object
        this.gl.shaderSource(shader, source);
        // Compile the shader program
        this.gl.compileShader(shader);
        // See if it compiled successfully
        if (!this.gl.getShaderParameter(shader, this.gl.COMPILE_STATUS)) {
            alert('An error occurred compiling the shaders: ' + this.gl.getShaderInfoLog(shader));
            this.gl.deleteShader(shader);
            return null;
        }
        return shader;
    }

    /**
     * Initialize a shader program, so WebGL knows how to draw our data
     * @param vsSource 
     * @param fsSource 
     * @returns 
     */
    public initShaderProgram(vsSource, fsSource) {
        const vertexShader = this.loadShader(this.gl.VERTEX_SHADER, vsSource);
        const fragmentShader = this.loadShader(this.gl.FRAGMENT_SHADER, fsSource);

        // Create the shader program
        const shaderProgram: WebGLProgram = this.gl.createProgram();
        this.gl.attachShader(shaderProgram, vertexShader);
        this.gl.attachShader(shaderProgram, fragmentShader);
        this.gl.linkProgram(shaderProgram);

        // If creating the shader program failed, alert
        if (!this.gl.getProgramParameter(shaderProgram, this.gl.LINK_STATUS)) {
            alert('Unable to initialize the shader program: ' + this.gl.getProgramInfoLog(shaderProgram));
            return null;
        }
        return shaderProgram;
    }

    /**
     * Draw the scene
     */
    public drawScene() {
        this.gl.clearColor(0.0, 0.0, 0.0, 0.0);         // Clear to defined color, fully opaque
        this.gl.clearDepth(1.0);                        // Clear everything
        this.gl.enable(this.gl.DEPTH_TEST);             // Enable depth testing
        this.gl.depthFunc(this.gl.LEQUAL);              // Near things obscure far things
        // Clear the canvas before we start drawing on it.
        this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);
        /**
         * Create a perspective matrix, a special matrix that is used to simulate the distortion of perspective in a camera.
        Our field of view is 45 degrees, with a width/height ratio that matches the display size of the canvas and we only want 
        to see objects between 0.1 units and 100 units away from the camera.
         */
        this.projectionMatrix = mat4.create();
        // note: glmatrix.js always has the first argument as the destination to receive the result.
        mat4.perspective(this.projectionMatrix, this.fieldOfView, this.aspect, this.zNear, this.zFar);
        // Set the drawing position to the "identity" point, which is
        // the center of the scene.
        this.modelViewMatrix = mat4.create();

        this.Mmatrix = mat4.create();
        // Now move the drawing position a bit to where we want to
        // start drawing the square.
        mat4.translate(this.modelViewMatrix,        // destination matrix
                        this.modelViewMatrix,       // matrix to translate
                        this.translate);            // amount to translate according [x, y, z]

        mat4.rotate(this.modelViewMatrix,           // destination matrix
                this.modelViewMatrix,               // matrix to rotate
                this.squareRotation,                // amount to rotate in radians
                this.rotate);                       // axis to rotate around

        // Tell Webgl how to pull out the positions from the position buffer into the vertexPosition attribute.
        {
            // const numComponents = 2;         
            const type = this.gl.FLOAT;         // the data in the buffer is 32bit floats
            const normalize = false;            // don't normalize
            const stride = 0;                   // how many bytes to get from one set of values to the next
                                                // 0 = use type and numComponents above
            const offset = 0;                   // how many bytes inside the buffer to start from
            this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.buffers.vertices);
            this.gl.vertexAttribPointer(
                this.programInfo.attribLocations.vertexPosition,
                // this.gl.getAttribLocation(this.programInfo, 'position'),
                this.numComponents.vertex,      // pull out 2 values per iteration
                type,
                normalize,
                stride,
                offset);
            this.gl.enableVertexAttribArray(  this.programInfo.attribLocations.vertexPosition );
        }
        
        // Tell Webthis.gl how to pull out the colors from the color buffer
        // into the vertexColor attribute.
        if (this.colors)
        {
            {
                const type = this.gl.FLOAT;
                const normalize = false;
                const stride = 0;
                const offset = 0;
                
                this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.buffers.colors);
                this.gl.vertexAttribPointer(
                    this.programInfo.attribLocations.vertexColor,
                    this.numComponents.color,
                    type,
                    normalize,
                    stride,
                    offset);
                this.gl.enableVertexAttribArray( this.programInfo.attribLocations.vertexColor);
            }
        }
        
        
        // Tell Webthis.gl to use our program when drawing
        this.gl.useProgram(this.programInfo.program);
        // Set the shader uniforms
        this.gl.uniformMatrix4fv(
            this.programInfo.uniformLocations.projectionMatrix,
            false,
            this.projectionMatrix);
        this.gl.uniformMatrix4fv(
            this.programInfo.uniformLocations.modelViewMatrix,
            false,
            this.modelViewMatrix);
        this.gl.uniformMatrix4fv(
            this.programInfo.uniformLocations.Mmatrix,
            false,
            this.mo_matrix);

        const offset = 0;
        if (this.indices){
            this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, this.buffers.indices);
            // this.gl.drawElements(this.gl.TRIANGLE_STRIP, this.indices.length, this.gl.UNSIGNED_SHORT, 0);
            this.gl.drawElements(this.gl.LINES, this.indices.length, this.gl.UNSIGNED_SHORT, 0);
    
        }
        else {
            this.gl.drawArrays(this.gl.TRIANGLE_STRIP, offset, this.vertexCount);
        }  
        this.squareRotation += this.deltaTime;
    }

    initializeView(angle: number, aspect: number, zNear: number, zFar: number, translate: number[], rotate: number[]){
        this.fieldOfView = angle;
        this.aspect = aspect; 
        this.zFar = zFar;
        this.zNear = zNear;
        this.translate = translate;
        this.rotate = rotate;
    }

    mouseEvents(){
        
        var canvas = this.gl.canvas;
        var AMORTIZATION = 0.95;
        var drag = false;
        var old_x, old_y;

        var mouseDown = function(e) {
            drag = true;
            old_x = e.pageX, old_y = e.pageY;
            e.preventDefault();
            return false;
        };

        var mouseUp = function(e){
            drag = false;
        };

        var mouseMove = function(e) {
            if (!drag) return false;
            this.dX = (e.pageX-old_x)*2*Math.PI/canvas.width;
            this.dY = (e.pageY-old_y)*2*Math.PI/canvas.height;
            this.THETA+= this.dX;
            this.PHI+=this.dY;
            old_x = e.pageX, old_y = e.pageY;
            e.preventDefault();
        };

        canvas.addEventListener("mousedown", mouseDown, false);
        canvas.addEventListener("mouseup", mouseUp, false);
        canvas.addEventListener("mouseout", mouseUp, false);
        canvas.addEventListener("mousemove", mouseMove, false);

        
    }

    rotateX(m, angle) {
        var c = Math.cos(angle);
        var s = Math.sin(angle);
        var mv1 = m[1], mv5 = m[5], mv9 = m[9];

        m[1] = m[1]*c-m[2]*s;
        m[5] = m[5]*c-m[6]*s;
        m[9] = m[9]*c-m[10]*s;

        m[2] = m[2]*c+mv1*s;
        m[6] = m[6]*c+mv5*s;
        m[10] = m[10]*c+mv9*s;
     }

    rotateY(m, angle) {
        var c = Math.cos(angle);
        var s = Math.sin(angle);
        var mv0 = m[0], mv4 = m[4], mv8 = m[8];

        m[0] = c*m[0]+s*m[2];
        m[4] = c*m[4]+s*m[6];
        m[8] = c*m[8]+s*m[10];

        m[2] = c*m[2]-s*mv0;
        m[6] = c*m[6]-s*mv4;
        m[10] = c*m[10]-s*mv8;
     }

    get_projection(angle, a, zMin, zMax) {
        var ang = Math.tan((angle*.5)*Math.PI/180);//angle*.5
        return [
            0.5/ang, 0 , 0, 0,
            0, 0.5*a/ang, 0, 0,
            0, 0, -(zMax+zMin)/(zMax-zMin), -1,
            0, 0, (-2*zMax*zMin)/(zMax-zMin), 0 
            ];
    }
}