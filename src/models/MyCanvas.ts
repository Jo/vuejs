export default class MyCanva 
{
  public id:string;
  public elt:object;
  public height:number;
  public width:number;
  public x:number;
  public y:number;


  constructor(id:string="", elt: object = {}, height:number=500, width:number=500, x:number=0, y:number=0 ){
    this.id = id
    this.elt = elt;
    this.height = height
    this.width = width
    this.x = x;
    this.y = y;
  }
}