export default class Person {
    prenom: string;
    nom: string;

    constructor(firstName: string, name: string){
        this.prenom = firstName;
        this.nom = name;
    }
}